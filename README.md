# java-vue

## Run Java
```
mvn jetty:run
```

## Setup Vue

### Install vue cli
npm install -g @vue/cli

### Create vue application
From command prompt, go to one folder above your project root

vue create "YourProjectRootFolderName"

	Select Merge
	

	Manually Select Features
		Typescript (Use arrow and spacekey to select)
			Class style syntax - Yes
			Use Babel - Yes
		ESLint + Prettier
		Lint on Save
		In Package.json
		Don't save preset
		
This will create a vue application inside your project folder and install all dependencies

### Setup Vue
Add a file vue.config.js to the root of your project and paste the below content
```
	module.exports = {
          outputDir: 'src/main/webapp/resources/dist',
          filenameHashing: false,
          configureWebpack: {
          optimization: {
                splitChunks: false
            },
            resolve: {
                alias: {
                    'vue$': 'vue/dist/vue.esm.js'
                }
            }
        },
    }
```
	
Open main.ts and replace with below
```
    import Vue from 'vue'
    Vue.config.productionTip = false

    new Vue({
    }).$mount('#vue_app')
```

### Build vue app
npm run build
	
### Include vue inside java app
Now open the main boot file in your application. In most cases it might be index.jsp

	Include css file. This wil be generated only if you have css inside your vue components
        ```
        <link href="./resources/dist/css/app.css" rel="stylesheet" />
        ```
	
	Include the bundled js file and the end of index.jsp inside bodytag
        ```
		<script src="./resources/dist/js/app.js"></script>
        ```
		
	vue_app should be the id of the html element inside which you are going to use the vue components
	
Now you can use vue components inside any of your jsp pages
	
### Creating vue components
	Create vue components inside "components" folder
	
	Register the component in main.ts
		Vue.component('home-page', require('./components/HomePage.vue').default);
	
	Use the component inside any jsp page
		<home-page />
