import Vue from 'vue'
import vuetify from '@/plugins/vuetify' 

Vue.config.productionTip = false
Vue.use(vuetify);

Vue.component('home-page', require('./components/HomePage.vue').default);


new Vue({
  vuetify
}).$mount('#vue_app')