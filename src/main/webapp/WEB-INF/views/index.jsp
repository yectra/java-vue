<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
    <head>
        <title>Sample Java Vue App</title>
        <link href="./resources/dist/css/app.css" rel="stylesheet" />
    </head>
    <body>
        <h1>${msg}</h1>
        <p>Today is <fmt:formatDate value="${today}" pattern="yyy-MM-dd" /></p>

        <h1>Vue</h1>
        <div id="vue_app">
            <jsp:include page="page.jsp"/>
        </div>

        <script src="./resources/dist/js/app.js"></script>
    </body>
</html>