module.exports = {
    outputDir: 'src/main/webapp/resources/dist',
    filenameHashing: false,
    configureWebpack: {
        optimization: {
            splitChunks: false
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js'
            }
        },
    },
    "transpileDependencies": [
        "vuetify"
    ]
}